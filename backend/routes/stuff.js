const express = require("express");
const router = express.Router();

const stuffCtrl = require('../controllers/stuff');

router.get('/', stuffCtrl.getAllStuff);
router.post('/', stuffCtrl.createThing);
router.get('/:id', stuffCtrl.getOneThing);
router.put('/:id', stuffCtrl.modifyThing);
router.delete('/:id', stuffCtrl.deleteThing);




// const Thing = require("../models/thing");

// router.post("/", (req, res, next) => {
//   const thing = new Thing({
//     title: req.body.title,
//     description: req.body.description,
//     imageUrl: req.body.imageUrl,
//     price: req.body.price,
//     userId: req.body.userId,
//   });
//   thing
//     .save()
//     .then(() => {
//       res.status(201).json({
//         message: "Post saved successfully !",
//       });
//     })
//     .catch((error) => {
//       res.status(400).json({
//         error: error,
//       });
//     });
// });

// router.get("/:id", (req, res, next) => {
//   Thing.findOne({
//     _id: req.params.id,
//   })
//     .then((thing) => {
//       res.status(200).json({
//         message: "Thing update successfully !",
//       });
//     })
//     .catch((error) => {
//       res.status(400).json({
//         error: error,
//       });
//     });
// });

// router.delete("/:id", (req, res, next) => {
//   Thing.deleteOne({
//     _id: req.params.id,
//   })
//     .then((thing) => {
//       res.status(200).json({
//         message: "Deleted !",
//       });
//     })
//     .catch((error) => {
//       res.status(400).json({
//         error: error,
//       });
//     });
// });

// router.get("/" + "", (req, res, next) => {
//   Thing.find()
//     .then((things) => {
//       res.status(200).json(things);
//     })
//     .catch((error) => {
//       res.status(400).json({
//         error: error,
//       });
//     });
// });

module.exports = router;

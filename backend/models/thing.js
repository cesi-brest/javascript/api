const mongoose = require('mongoose');
//Génère un schema dans la BDD 
//On n'a pas besoin de créer d'ID il est directement géré par Mongoose
const thingSchema = mongoose.Schema({
    title: { type: String, required: true },
    description: { type: String, required: true },
    imageUrl: { type: String, required: true },
    userId: { type: String, required: true },
    price: { type: Number, required: true },
});
//Exportation du schéma en tant que model
module.exports = mongoose.model('Thing', thingSchema);



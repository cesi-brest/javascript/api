const express = require('express');

const app = express();
app.use(express.json());

const mongoose = require('mongoose');
const Thing = require('./models/thing');
const User = require('./models/user');


const stuffRoutes = require('./routes/stuff');
const userRoutes = require('./routes/user');

// Mongoose sera avant les headers aussi
mongoose.connect('mongodb://localhost:27017/marketSell',
{ useNewUrlParser: true, useUnifiedTopology: true })
.then(() => console.log('Connexion à MongoDB réussie ! '))
.catch(() => console.log('Connexion à MongoDB échoutée ! '));


// Les headers sont avant les routes
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});

app.use('/api/stuff', stuffRoutes);


// On place les routes 'post' avant les uses ou les 'get' car l'application interceptera les méthodes 'get' car on ne leur a donné aucun verbe HTTP spécifique
// On place les plus spécifiques au début, et ensuite les moins spécifiques


// app.use('/api/stuff', (req,res,next) => {
//     const stuff = [ //Rajout de la route avec une chaîne de caractères
//         {
//             _id: 'mplea',
//             title: 'Mon premier objet',
//             description: 'Les infos de mon premier objet',
//             imageUrl: 'https://media.istockphoto.com/id/1157326018/fr/photo/ampoule-lumineuse-%C3%A9clatante-se-d%C3%A9taquier-de-la-foule.jpg?s=2048x2048&w=is&k=20&c=oPK43oGia7ffwKUCVJpWgtzyogg1XfNUXmOjjt9iFgs=',
//             price: 4900,
//             userId: 'erazel'
//         },
//         {
//             _id: 'eface',
//             title: 'Mon deuxième objet',
//             description: 'Les infos de mon deuxième objet',
//             imageUrl: 'https://cdn.pixabay.com/photo/2022/09/14/18/32/cafe-7454951_1280.jpg',
//             price: 2900,
//             userId: 'koliap'
//         },
//     ];
//     res.status(200).json(stuff);
// });


// app.use((req, res, next) => {
//     console.log('Requête reçue !');
//     next();
// });

// app.use((req, res, next) => {
//     res.status(201);
//     next();
// });

// app.use((req, res, next) => {
//     res.json({message : 'Votre requête a bien été reçue !'});
//     next();
// });

// app.use((req, res, next) => {
//     console.log('Réponse envoyée avec succès');
// });



// app.get('/api/stuff/:id', (req, res, next) => {
//     console.log('GetById')
//     Thing.findOne({ _id: req.params.id })
//     .then(thing => res.status(200).json(thing))
//     .catch(error => res.status(404).json({ error }));
// });

// app.post('/api/stuff', (req, res, next) => {
//     console.log('Create')
//     delete req.body._id;
//     const thing = new Thing({
//         ...req.body
//     });
//     thing.save()
//     .then(() => res.status(201).json({ message: 'Objet enregistré !'}))
//     .catch(error => res.status(400).json({ error }));
// });

//Mise à jour de l'entité
//Ici on est sur une structure donc pas de New, sinon Mongoose génèrera un nouvel ID donc une erreur. 
// app.put('/api/stuff/:id', (req, res, next) => {
//     console.log('UpdateById')
//     Thing.updateOne({ _id: req.params.id }, { ...req.body, _id: req.params.id })
//     .then(() => res.status(200).json({ message: 'Objet modifié'}))
//     .catch(error => res.status(400).json({ error }));
// });

// app.delete('/api/stuff/:id', (req, res, next) => {
//     console.log('DeleteById')
//     Thing.deleteOne({ _id: req.params.id })
//     .then(() => res.status(200).json({ message : 'Objet supprimé !' }))
//     .catch(error => res.status(400).json({ error }));
// });

// app.use('/api/stuff', (req, res, next) => {
//     Thing.find()
//     .then(things => res.status(200).json(things))
//     .catch(error => res.status(400).json({ error }));
// });
app.use('/api/auth', userRoutes);

module.exports = app;